# Gaining Load Balancer Certificate Visibility Leveraging the Citrix Nitro API

# Purpose

This repository is used to provide a way to gain visibility into load balancer certificates inside of NetScaler devices. The script provided in this repository will not only gather certificate information, but will also identify where the certificate is being used on the NetScaler.

# Setup

This repository requires very little setup to get working. There is a provided requirements.txt file that can be used to setup a virtual environment for running the application.

# Blogs

There is a blog that has been written to go along with this repository, which can be found below:
- [Blog]()