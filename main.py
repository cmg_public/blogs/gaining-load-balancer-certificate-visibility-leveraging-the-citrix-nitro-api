# Imports
import ssl
import socket
from dotenv import load_dotenv
import requests
import os
import logging
from dateutil.parser import parse

load_dotenv()  # Load environment variables

# Define variables for API authentication
ns_username = os.environ.get('NITRO_USERNAME')
ns_password = os.environ.get('NITRO_PASSWORD')

# Define headers for API requests

ns_headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'X-NITRO-USER': ns_username,
    'X-NITRO-PASS': ns_password
}

netscalers = ['10.20.20.20']
certificate_data = dict()  # Initialize an empty list for certificates

for netscaler in netscalers:  # Iterate through each NetScaler
    certificate_data[netscaler] = dict()  # Initialize a dictionary that ties back to the netscaler
    url = f'https://{netscaler}'  # Define the URL

    logging.info(f'** Processing {netscaler} **')
    logging.info(f'Gathering SSL Cert Keys')

    response = requests.get(f'{url}/nitro/v1/config/sslcertkey', headers=ns_headers, verify=False)  # Get a list of cert keys

    if response.status_code == 200:  # If the request was successful
        logging.info(f'Cert Keys Successfully Gathered')
        cert_keys = response.json()['sslcertkey']  # Parse the cert keys from the response
        cert_keys = [x for x in cert_keys if x['certkey'] != 'ns-server-certificate' and x.get('key')
                     and x['status'] != 'Expired']  # Filter out unwanted cert keys

        logging.info(f'Processing {len(cert_keys)} Cert Keys')

        for cert in cert_keys:  # Iterate through each cert key
            cert_key = cert['certkey']  # Define the cert key
            certificate_data[netscaler][cert_key] = dict()  # Initialize a dictionary that ties back to the netscaler cert key

            if 'cn=' in cert['subject'].lower():
                serial_number = cert['serial']  # Define the serial number of the certificate
                common_name = [x.split('=')[1] for x in cert['subject'].split(',') if 'cn=' in x.lower()][0].split('/')[0]  # Define the common name of the cert using the Subject

                try:
                    logging.info(f'Resolving {common_name}')

                    ctx = ssl.create_default_context()  # Create an SSL context to try and get additional SSL certificate information from the common name parsed from the subject
                    with ctx.wrap_socket(socket.socket(), server_hostname=common_name) as s:
                        s.connect((common_name, 443))  # Try an SSL connection over port 443 to see if the certificate information could be obtained
                        ssl_cert = s.getpeercert()  # Get the additional certificate information
                except:
                    ssl_cert = None  # If the connection was unsuccessful, then set the variable as such

                if ssl_cert:  # If the additional certificate information was obtained, get the Subject Alternative Names for the certificate
                    san = [x[-1] for x in ssl_cert['subjectAltName']]
                else:
                    san = cert.get('sandns')  # Try to get the SAN based on what is returned from the Nitro API

                if san and type(san) != list:  # If the SAN only had a single item
                    san = ','.join([san])
                elif type(san) == list:  # Otherwise, if it had multiple items
                    san = ','.join(san)

                certificate_info = {
                    'cn': common_name,
                    'san': san,
                    'sn': serial_number,
                    'expiration_date': str(parse(cert['clientcertnotafter']).date()),
                }  # Add the certificate found on the NetScaler to the certificate list

                certificate_data[netscaler][cert_key]['certificate'] = certificate_info  # Add the certificate information to the certificates list

            response = requests.get(f'{url}/nitro/v1/config/sslcertkey_binding/{cert_key}',
                                    headers=ns_headers, verify=False)  # Query for all bindings pertaining to the cert key

            if response.status_code == 200:  # If the request was successful
                keys = {
                    'sslcertkey_sslvserver_binding': 'vserver',
                    'sslcertkey_service_binding': 'service'
                }  # Define the key mappings

                binding_list = response.json()['sslcertkey_binding']  # Parse out the bindings from the response
                binding_dict = {k: v for x in binding_list for k, v in x.items() if k != 'certkey'}  # Remove unnecessary keys
                binding_keys = [x for x in binding_dict.keys() if x != 'certkey']  # Remove unnecessary keys
                cert_binding = dict()  # Initialize an empty dictionary

                for key in binding_keys:  # Iterate through each key
                    if key in keys.keys():  # If the key exists in the dictionary
                        cert_binding[keys[key]] = [v for x in binding_dict[key] for k, v in x.items() if 'name' in k]  # Map the old keys to the new keys

                if cert_binding:
                    certificate_data[netscaler][cert_key]['bindings'] = cert_binding
            else:
                logging.error(f'Unable to gather binding information on certificate with cert key - {cert_key}')
    else:
        logging.error(f'Unable to gather certificates on {netscaler}. {response.status_code} - {response.content}')
